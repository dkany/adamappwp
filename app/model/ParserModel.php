<?php
/**
 * Created by PhpStorm.
 * User: tomas
 * Date: 23.2.16
 * Time: 15:57
 */

namespace App\Model;

use \Kdyby\Curl,
    Nette\Utils\DateTime;
use Nette\Neon\Exception;

class ParserModel {

    const MOBAPS_DOMAIN = "http://my.adamapp.com/";

    /*
     * Load and decode data from JSON
     */
    public function loadData($app_id){
        return json_decode(file_get_contents('http://my.adamapp.com/export/web/' . $app_id));
    }

    /*
     * Get name of the web (title)
     */
    public function getName($data){
        return $data->result->data->config->title;
    }

    /*
     * find external link
     */
    public function findExternalLink($tabs, $id){
        $externalTab = $this->findTabByID($tabs, $id);
        return $externalTab->link;
    }

    /*
    * get type of submenu - dropdown or singlepage
    */
    public function getSubMenuType($tabs, $tabID){

        foreach($tabs as $tab){
            if($tab->id == $tabID && $tab->type->name == "menu"){
                return $tab->type->web_submenu;
            }
        }

        return 0;
    }

    /*
     * Get Menu with submenus up to level 3
     *
     */
    public function getMenu($data){
        $tabs = $data->result->data->tab;
        $main_menu = $data->result->data->main_tab;
        $menu = array();

        foreach($tabs as $tab){
            if($tab->id == $main_menu){
                foreach($tab->menu->item as $item){
                    if($item->name != "Rozdělovník") {
                        $subMenu = $this->getSubMenu($tabs, $item->tab);

                        if (empty($subMenu)) {
                            $has_sub = 0;
                        } else {
                            $has_sub = 1;
                        }

                        $sub_type = $this->getSubMenuType($tabs, $item->tab);

                        if($item->tab_type == "link_external"){
                            $external_link = $this->findExternalLink($tabs, $item->tab);
                            $menu[] = array(
                                "id" => $item->id,
                                "name" => $item->name,
                                "type" => $item->tab_type,
                                "link" => $external_link,
                                "has_sub" => $has_sub,
                                "sub_type" => $sub_type,
                                "sub_menu" => $subMenu
                            );
                        } else {
                            $menu[] = array(
                                "id" => $item->id,
                                "name" => $item->name,
                                "type" => $item->tab_type,
                                "link" => $item->tab,
                                "has_sub" => $has_sub,
                                "sub_type" => $sub_type,
                                "sub_menu" => $subMenu
                            );
                        }
                    }

                }
                break;
            }
        }

        return $menu;
    }

    /*
     * Get submenu for parent, where parentID is the parameter
     */
    private function getSubMenu($tabs, $parentID){
        $subMenu = array();
        foreach($tabs as $tab){
            $type = "";
            if(isset($tab->type)){
                $type = $tab->type->name;
            }
            if($tab->id == $parentID && ($type === "menu")){
                foreach($tab->menu->item as $item){

                    $subSubMenu = $this->getSubSubMenu($tabs, $item->tab);

                    if(empty($subSubMenu)){
                        $has_sub = 0;
                    } else {
                        $has_sub = 1;
                    }

                    $sub_type = $this->getSubMenuType($tabs, $item->tab);

                    if($item->tab_type == "link_external"){

                        $external_link = $this->findExternalLink($tabs, $item->tab);
                        $subMenu[] = array(
                            "id" => $item->id,
                            "name" => $item->name,
                            "type" => $item->tab_type,
                            "link" => $external_link,
                            "has_sub" => $has_sub,
                            "sub_type" => $sub_type,
                            "sub_sub_menu" => $subSubMenu
                        );

                    } else {
                        $subMenu[] = array(
                            "id" => $item->id,
                            "name" => $item->name,
                            "type" => $item->tab_type,
                            "link" => $item->tab,
                            "has_sub" => $has_sub,
                            "sub_type" => $sub_type,
                            "sub_sub_menu" => $subSubMenu
                        );
                    }
                }
            }
        }

        return $subMenu;
    }

    /*
     * Get sub_sub_menu for sub_menu
     */
    private function getSubSubMenu($tabs, $parentID){
        $subSubMenu = array();
        foreach($tabs as $tab){
            if($tab->id == $parentID && $tab->type->name == "menu"){
                foreach($tab->menu->item as $item){

                    if($item->tab_type == "link_external"){
                        $external_link = $this->findExternalLink($tabs, $item->tab);
                        $subSubmenu[] = array(
                            "id" => $item->id,
                            "name" => $item->name,
                            "type" => $item->tab_type,
                            "link" => $external_link
                        );
                    } else {
                        $subSubMenu[] = array(
                            "id" => $item->id,
                            "name" => $item->name,
                            "type" => $item->tab_type,
                            "link" => $item->tab
                        );
                    }
                }
            }
        }
        return $subSubMenu;
    }

    /*
     * Find the tab by web_class name
     */
    private function getTabByClass($tabs, $web_class){
        foreach($tabs as $tab){
            if($tab->web_class == $web_class){
                return $tab;
            }
        }

        return false;
    }

    /*
     * Get contact for homepage
     */
    public function getContact($data){

        foreach($data->result->data->tab as $tab){
            if($tab->web_class == "contact"){
                $contactTab = $tab;
            }
        }

        if(isset($contactTab)) {

            $section_id = $contactTab->id;
            $flag = 0;
            foreach ($contactTab->unified_content->item as $item) {
                if (isset($item->type->name)) {
                    if($item->type->name == "button") {
                        if(isset($item->action->type->name)) {
                            if ($item->action->type->name == "call" && $flag == 0) {
                                $phone = $item->action->link;
                                $flag = 1;
                            }
                            if ($item->action->type->name == "mail") {
                                $mail = $item->action->link;
                            }
                        }
                    }
                }
            }
        }

        if(isset($phone) && isset($mail) && isset($section_id)){
            return array("phone" => $phone, "mail" => $mail, "id" => $section_id);
        } else {
            return array();
        }
    }

    /*
     * Get Gallery
     */
    public function getGallery($data){
        $tabs = $data->result->data->tab;
        $galleryTab = $this->getTabByClass($tabs, "gallery");

        if($galleryTab->type->name == "menu") {
            $categories = array();
            $images = array();
            if ($galleryTab != false) {
                foreach ($galleryTab->menu->item as $category) {
                    foreach ($tabs as $tab) {
                        if ($tab->id === $category->tab) {
                            if (isset($tab->unified_content->item[0]->image)) {
                                $imageArray = $tab->unified_content->item[0]->image;
                                foreach ($imageArray as $image) {
                                    $images[] = array(
                                        "src" => $image->src,
                                        "alt" => $image->title
                                    );
                                }
                            }
                        }
                    }

                    $categories[] = array(
                        "link" => $category->tab,
                        "name" => $category->name,
                        "images" => $images
                    );

                    if (isset($images)) {
                        unset($images);
                    }

                }
                return $categories;
            }
            return null;
        } elseif($galleryTab->type->name == "content"){
            $imageArray = array();
            foreach($galleryTab->unified_content->item[0]->image as $item){
                $imageArray[] = array(
                    "src" => self::MOBAPS_DOMAIN . $item->src,
                    "alt" => $item->title
                );
            }

            return $imageArray;
        } else {
            return null;
        }
    }

    /*
     *  return gallery type
     */
    public function getGalleryType($data){
        $tabs = $data->result->data->tab;
        $galleryTab = $this->getTabByClass($tabs, "gallery");
        if($galleryTab->type->name == "menu") {
            return "multi";
        } elseif ($galleryTab->type->name == "content"){
            return "single";
        } else {
            return null;
        }
    }

    /*
     * Get coordinates for google map - lat and lng
     */
    public function getCoords($data){
        $coords = $data->result->data->config->map_gps;
        $lat = substr($coords, 0, 9);
        $lng = substr($coords, 11, 9);

        return array("lat" => floatval($lat), "lng" => floatval($lng));
    }

    /*
     * Get color schema
     */
    public function getColorSchema($data){
        $colors = $data->result->data->config->color_schema;
        return array("action" => $colors->action,
                     "action2" => $colors->action2,
                     "text" => $colors->text,
                     "bg_menu" => $colors->bg_menu,
                     "font_menu" => $colors->font_menu,
                     "bg_page" => $colors->bg_page,
                     "font_page" => $colors->font_page,
                     "top3bg" => $colors->top3_bg,
                     "footer" => $this->hex2rgba($colors->bg_menu, 0.8));
    }

    /*
     * Get logo of the page
     */
    public function getLogo($data){
        return self::MOBAPS_DOMAIN . $data->result->data->config->logo->src;
    }

    /*
     * Get Favicon for the web
     */
    public function getFavicon($data){
        return self::MOBAPS_DOMAIN . $data->result->data->config->favicon->src;
    }

    /*
     * find and return splitter image
     */
    public function getSplitter($data){
        $splitterTab = $this->getTabByClass($data->result->data->tab, "splitter");

        if($splitterTab != false){
            if($splitterTab->unified_content->item[0]->image[0]->src){
                return self::MOBAPS_DOMAIN . $splitterTab->unified_content->item[0]->image[0]->src;
            } else {
                return null;
            }
        }
        return null;
    }

    /*
     * Parse images from image array
     */
    private function getImages($imageArray){
        $images = array();
        foreach($imageArray as $image){
            $images[] = array(
                "img" => $image->src,
                "alt" => $image->title
            );
        }
        return $images;
    }

    /*
     * Parse form item
     */
    private function parseFormItem($item){
        $formItem = array(
            "text" => $item->text,
            "name" => $item->form->name,
            "req" => $item->form->required,
            "type" => $item->form->type->name
        );

        if($item->form->type->name == "radio"){
            $select = array();
            foreach($item->form->selections as $selectItem){
                $select[$selectItem->id] = $selectItem->text;

            }
            $formItem["select"] = $select;
        }

        if($item->form->type->name == "products"){
            $products = array();
            foreach($item->form->selections as $selectItem){
                $products[$selectItem->id] = array(
                    "name" => $selectItem->name,
                    "text" => $selectItem->text,
                    "price" =>  $selectItem->price_,
                    "price_name" => $selectItem->price_name
                );
            }
            $formItem["products"] = $products;
        }

        return $formItem;
    }

    /*
     * Parse unified_content from JSON
     */
    private function parseContent($contentArray, $tabID){
        $content = array();
        $form = array();
        foreach($contentArray as $item){
            $name = $item->type->name;
            switch ($name){
                case "header":
                    $content[] = array(
                        "type" => $name,
                        $name => $item->text
                    );
                    break;
                case "text":
                   $content[] = array(
                        "type" => $name,
                        $name => $item->text
                    );
                    break;
                case "image":
                    $content[] = array(
                        "type"=> $name,
                        "images" => $this->getImages($item->image)
                    );
                    break;
                case "html":
                    $content[] = array(
                        "type"=> $name,
                        $name => $item->text
                    );
                    break;
                case "form":
                    $form[] = $this->parseFormItem($item);
                    break;
                case "button":
                    if(isset($item->action->type->name)) {
                        if ($item->action->type->name == "form") {
                            $form[] = array(
                                "text" => $item->text,
                                "type" => "send",
                                "url" => $item->action->form->url
                            );
                        } elseif ($item->action->type->name == "call") {
                            $content[] = array(
                                "text" => $item->text,
                                "type"=> $item->action->type->name,
                                "link" => $item->action->link
                            );
                        } elseif ($item->action->type->name == "mail") {
                            $content[] = array(
                                "text" => $item->text,
                                "type"=> $item->action->type->name,
                                "link" => $item->action->link
                            );
                        } elseif($item->action->type->name == "url"){
                            $content[] = array(
                                "text" => $item->text,
                                "type" => $item->action->type->name,
                                "link" => $item->action->link
                            );
                        } elseif ($item->action->type->name == "facebook"){
                            $content[] = array(
                                "text" => $item->text,
                                "type" => $item->action->type->name,
                                "link" => $item->action->link
                            );
                        }
                    }
                    break;
            }
        }

        if(!empty($form)){
            $content[] = array(
                "type" => "form",
                "ID" => $tabID,
                "form" => $form
            );
        }
        return $content;
    }

    /*
     * create content page for menu item
     */
    private function getContentForMenuItem($tabs, $link){
        $content = false;
        foreach($tabs as $tab){
            if($tab->id === $link) {
                if(isset($tab->web_class)){
                    $web_class = $tab->web_class;
                } else {
                    $web_class = "";
                }

                if ($web_class != "gallery" && $web_class != "claim" && $web_class != "splitter") {
                    if(isset($tab->type->name)){
                        $new_name = $tab->type->name;
                    } else {
                      $new_name = "";
                    }
                    switch ($new_name) {
                        case "content":
                            $content = array("id" => $link,
                                "type" => "content",
                                "content" => $this->parseContent($tab->unified_content->item, $tab->id));
                            break;
                        case "link":
                            $content = array("id" => $link,
                                "type" => "link",
                                "link_type" => $tab->type->link_type,
                                "link" => $tab->link,
                                "name" => $tab->web_name);
                            break;
                        case "button":
                            $content[] = array(
                                "type" => $item->type->name,
                                "text" => $item->text,
                                "link" => $item->action->link
                            );
                            break;
                    }
                }
            }
        }
        return $content;
    }

    /*
      * Creates pages for menu items
    */
    public function createPages($data){
        $menu = $this->getMenu($data);
        $tabs = $data->result->data->tab;

        $pages = array();
        foreach($menu as $item) {
            if ($item["has_sub"] === 1) {
                if($item["sub_type"] == 1) {
                    foreach ($item["sub_menu"] as $subItem) {
                        if ($subItem["has_sub"] === 1) {
                            if ($subItem["sub_type"] == 1) {
                                foreach ($subItem["sub_sub_menu"] as $subSubItem) {
                                    if ($this->getContentForMenuItem($tabs, $subSubItem["link"]))
                                        $pages[] = $this->getContentForMenuItem($tabs, $subSubItem["link"]);
                                }
                            }
                        } else {
                            if ($this->getContentForMenuItem($tabs, $subItem["link"]))
                                $pages[] = $this->getContentForMenuItem($tabs, $subItem["link"]);
                        }
                    }
                }
            } else {
                if ($this->getContentForMenuItem($tabs, $item["link"]))
                    $pages[] = $this->getContentForMenuItem($tabs, $item["link"]);
            }
        }

        return $pages;
    }

    /*
     * Get Forms from pages
     */
    public function selectForms($pages){
        $forms = array();

        foreach($pages as $page){
            if($page["type"] == "content"){
                foreach($page["content"] as $content){
                    if($content["type"] == "form"){
                       $forms[$content["ID"]] = array(
                           "ID" => $content["ID"],
                           "form" => $content["form"]
                       );
                    }
                }
            }
        }

        return $forms;
    }

    /*
     * Get claims for homepage
     */
    public function getClaims($data){
        $tabs = $data->result->data->tab;
        $claims = array();
        foreach($tabs as $tab){
            if($tab->web_class == "claim"){
                $flag = 1;
                foreach($tab->unified_content->item as $item){
                    if($item->type->name == "image"){
                        foreach($item->image as $image){
                            $claims[] = array (
                                "text" => $image->title,
                                "img" => self::MOBAPS_DOMAIN . $image->src . '&width=1920'
                            );
                        }
                    }
                }
            }
        }
        if(isset($flag)){
            return $claims;
        } else {
            return null;
        }

    }

    /*
     * Get footer button
     */
    public function getFooterButton($data){
        $link =  $data->result->data->config->footer_button_tab;
        $main_tab = $data->result->data->main_tab;

        foreach($data->result->data->tab as $tab){
            if($tab->id == $link){
                    if($tab->type->name == "link"){
                        return array("link" => $tab->link,
                                     "type" => "external",
                                     "name" => $tab->web_name);
                    } else {
                        return array("link" => $tab->id,
                            "type" => "internal",
                            "name" => $tab->web_name);
                    }
            }
        }

    }

    /*
     * Get contact info for footer
     */
    public function getFooterContact($data){
        return $data->result->data->config->address;
    }

    /*
     * Find tab by ID
     */
    private function findTabByID($tabs, $id){
        foreach($tabs as $tab){
            if($tab->id == $id){
                return $tab;
            }
        }
    }

    /*
     * Creates Top section
     */
    public function getTopSection($data){
        $topTab = $this->getTabByClass($data->result->data->tab, "top");
        $tops = array();

        if($topTab != false) {
            foreach ($topTab->menu->item as $item) {
                $itemTab = $this->findTabByID($data->result->data->tab, $item->tab);

                foreach ($itemTab->unified_content->item as $topItem) {
                    switch ($topItem->type->name) {
                        case "header":
                            $title = $topItem->text;
                            break;
                        case "text":
                            $text = $topItem->text;
                            break;
                        case "image":
                            $image = array("src" => $topItem->image[0]->src,
                                "alt" => $topItem->image[0]->title);
                            break;
                        case "button":
                            $button = array("text" => $topItem->text,
                                "link" => $topItem->action->link);
                            break;
                        case "html":
                            $html = $topItem->text;
                            break;
                    }
                }

                if (isset($title) && isset($text) && isset($image) && isset($button)) {
                    $tops[] = array(
                        "title" => $title,
                        "text" => $text,
                        "image" => $image,
                        "button" => $button
                    );
                } elseif (isset($title) && isset($image) && isset($html) && isset($button)){
                    $tops[] = array(
                        "title" => $title,
                        "html" => $html,
                        "image" => $image,
                        "button" => $button
                    );
                } elseif(!isset($button)) {
                    $tops[] = array(
                        "title" => $title,
                        "html" => $html,
                        "image" => $image
                    );
                }
                unset($title);
                unset($text);
                unset($image);
                unset($button);
                unset($html);
            }

            return $tops;
        } else {
            return null;
        }
    }

    /*
     * Parse date and time for open hours
     */
    private function parseTime($time, $day){

        if($time != null && $time != "zavřeno"){
            $from = substr( $time, 0, 5);
            $to = substr( $time, -5);

            return array("day" => $day,
                "from" => $from,
                "to" => $to);
        } else {
            return array("day" => "closed",
                "from" => 0,
                "to" => 0);
        }

    }

    /*
     *  Return Open hours
     */
    public function getOpenHours($data){

        $hours = array();
        $hours[] = $this->parseTime($data->result->data->config->open_hours->monday, "Monday");
        $hours[] = $this->parseTime($data->result->data->config->open_hours->tuesday, "Tuesday");
        $hours[] = $this->parseTime($data->result->data->config->open_hours->wednesday, "Wednesday");
        $hours[] = $this->parseTime($data->result->data->config->open_hours->thursday, "Thursday");
        $hours[] = $this->parseTime($data->result->data->config->open_hours->friday, "Friday");
        $hours[] = $this->parseTime($data->result->data->config->open_hours->saturday, "Saturday");
        $hours[] = $this->parseTime($data->result->data->config->open_hours->sunday, "Sunday");

        return $hours;
    }

    /*
     * Check if the app does have open hours
     */
    public function hasHours($data){

        $hours = array();
        $hours[] = $data->result->data->config->open_hours->monday;
        $hours[] = $data->result->data->config->open_hours->tuesday;
        $hours[] = $data->result->data->config->open_hours->wednesday;
        $hours[] = $data->result->data->config->open_hours->thursday;
        $hours[] = $data->result->data->config->open_hours->friday;
        $hours[] = $data->result->data->config->open_hours->saturday;
        $hours[] = $data->result->data->config->open_hours->sunday;

        $counter = 0;
        for($i = 0; $i < 7; $i++){
            if($hours[$i] == null){
                $counter++;
            }
        }

        if($counter == 7){
            return false;
        } else {
            return true;
        }

    }

    /*
     * Gets openhours for homepage
     */
    public function getHours($data, $lang){
        if($lang == "cs"){
            $hours = array();
            if($data->result->data->config->open_hours->monday != null){
                $hours[] =  array("day" => "Pondělí", "value" => $data->result->data->config->open_hours->monday);
            }
            if($data->result->data->config->open_hours->tuesday != null){
                $hours[] =  array("day" => "Úterý", "value" => $data->result->data->config->open_hours->tuesday);
            }
            if($data->result->data->config->open_hours->wednesday != null){
                $hours[] =  array("day" => "Středa", "value" => $data->result->data->config->open_hours->wednesday);
            }
            if($data->result->data->config->open_hours->thursday != null){
                $hours[] =  array("day" => "Čtvrtek", "value" => $data->result->data->config->open_hours->thursday);
            }
            if($data->result->data->config->open_hours->friday != null){
                $hours[] =  array("day" => "Pátek", "value" => $data->result->data->config->open_hours->friday);
            }
            if($data->result->data->config->open_hours->saturday != null){
                $hours[] =  array("day" => "Sobota", "value" => $data->result->data->config->open_hours->saturday);
            }
            if($data->result->data->config->open_hours->sunday != null){
                $hours[] =  array("day" => "Neděle", "value" => $data->result->data->config->open_hours->sunday);
            }

            return $hours;
        } else {
            $hours = array();
            if($data->result->data->config->open_hours->monday != null){
                $hours[] =  array("day" => "Monday", "value" => $data->result->data->config->open_hours->monday);
            }
            if($data->result->data->config->open_hours->tuesday != null){
                $hours[] =  array("day" => "Tuesday", "value" => $data->result->data->config->open_hours->tuesday);
            }
            if($data->result->data->config->open_hours->wednesday != null){
                $hours[] =  array("day" => "Wednesday", "value" => $data->result->data->config->open_hours->wednesday);
            }
            if($data->result->data->config->open_hours->thursday != null){
                $hours[] =  array("day" => "Thursday", "value" => $data->result->data->config->open_hours->thursday);
            }
            if($data->result->data->config->open_hours->friday != null){
                $hours[] =  array("day" => "Friday", "value" => $data->result->data->config->open_hours->friday);
            }
            if($data->result->data->config->open_hours->saturday != null){
                $hours[] =  array("day" => "Saturday", "value" => $data->result->data->config->open_hours->saturday);
            }
            if($data->result->data->config->open_hours->sunday != null){
                $hours[] =  array("day" => "Sunday", "value" => $data->result->data->config->open_hours->sunday);
            }

            return $hours;
        }
    }

    /*
     * Get a pop-up window
     */
    public function getPopUp($data){
        $popupTab = $this->getTabByClass($data->result->data->tab, "popup");

        $popup = array();
        if($popupTab != false) {
            foreach ($popupTab->unified_content->item as $item) {
                switch ($item->type->name) {
                    case "text":
                        $text = $item->text;
                        break;
                    case "image":
                        $image = array("src" => $item->image[0]->src,
                            "alt" => $item->image[0]->title);
                        break;
                }
                if (isset($text)) {
                    $popup["text"] = $text;
                }
                if (isset($image)) {
                    $popup["image"] = $image;
                }
            }
        }

        if(isset($text) || isset($image)){
            return $popup;
        } else {
            return null;
        }

    }

    /*
     * colors converter
     */
    private function hex2rgba($color, $opacity = false) {

        $default = 'rgb(0,0,0)';

        if(empty($color))
            return $default;

        if ($color[0] == '#' ) {
            $color = substr( $color, 1 );
        }

        if (strlen($color) == 6) {
            $hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
        } elseif ( strlen( $color ) == 3 ) {
            $hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
        } else {
            return $default;
        }

        $rgb =  array_map('hexdec', $hex);

        if($opacity){
            if(abs($opacity) > 1)
                $opacity = 1.0;
            $output = 'rgba('.implode(",",$rgb).','.$opacity.')';
        } else {
            $output = 'rgb('.implode(",",$rgb).')';
        }

        //Return rgb(a) color string
        return $output;
    }

    /*
     * return top 3 section
     */
    public function getTop3($data){
        $top3Tab = $this->getTabByClass($data->result->data->tab, "top3");
        $tops = array();
        if($top3Tab != false) {
            foreach ($top3Tab->menu->item as $item) {
                $itemTab = $this->findTabByID($data->result->data->tab, $item->tab);

                foreach ($itemTab->unified_content->item as $topItem) {
                    switch ($topItem->type->name) {
                        case "header":
                            $title = $topItem->text;
                            break;
                        case "text":
                            $text = $topItem->text;
                            break;
                        case "image":
                            if (isset($topItem->image[0]->src)) {
                                $image = array("src" => self::MOBAPS_DOMAIN . $topItem->image[0]->src,
                                    "alt" => $topItem->image[0]->title);
                            } else {
                                $image = array("src" => self::MOBAPS_DOMAIN . $data->result->data->config->placeholder->src,
                                    "alt" => "placeholder");
                            }
                            break;
                        case "html":
                            $html = $topItem->text;
                            break;
                    }
                }
                if (!isset($image)) {
                    $image = array("src" => self::MOBAPS_DOMAIN . $data->result->data->config->placeholder->src,
                        "alt" => "placeholder");
                }
                if (isset($html)) {
                    if (isset($title) && isset($image)) {
                        $tops[] = array(
                            "id" => $item->tab,
                            "title" => $title,
                            "image" => $image,
                            "html" => $html
                        );
                    }
                } else {
                    if (isset($title) && isset($image) && isset($text)) {
                        $tops[] = array(
                            "id" => $item->tab,
                            "title" => $title,
                            "image" => $image,
                            "text" => $text
                        );
                    }
                }

                unset($title);
                unset($text);
                unset($image);
                unset($html);
            }
            return $tops;
        } else {
            return NULL;
        }
    }

    /*
     * Get top3 title
     */
    public function getTop3Title($data){
        $top3Tab = $this->getTabByClass($data->result->data->tab, "top3");
        return $top3Tab->web_name;
    }

    /*
     * return gallery title
     */
    public function getGalleryTitle($data){
        $galleryTab = $this->getTabByClass($data->result->data->tab, "gallery");
        if($galleryTab != false){
            return $galleryTab->web_name;
        } else {
            return null;
        }

    }

    /*
     * Get font family
     */
    public function getFontFamily($data){
        return str_replace(" ", "+", $data->result->data->config->font->name);
    }

    /*
    * Get font name
    */
    public function getFontName($data){
        return $data->result->data->config->font->name;
    }

    /*
     * get font types
     */
    public function getFontTypes($data){
        $types = array();
        foreach($data->result->data->config->font->types as $type){
            if($type == "regular"){
                $types[] = "400";
            } elseif ($type == "italic"){
                $types[] = "400italic";
            } else {
                $types[] = $type;
            }
        }

        return $types;
    }

    public function getContactText($data){
        return $data->result->data->config->contact_text;
    }

    public function getNumberOfMenuItems($menu){
        $c = 0;
        foreach($menu as $item){
            $c++;
        }
        return $c;
    }

    public function getStoreLinks($data){
        $links = array();
        if(isset($data->result->data->links->google)){
            $links["google"] = $data->result->data->links->google;
        }
        if(isset($data->result->data->links->apple)){
            $links["apple"] = $data->result->data->links->apple;
        }
        return $links;
    }

     /*
     * Get pages with subItems - different type of submenus
     */
    public function getSubTypePage($data){
        $menu = $this->getMenu($data);
        $tabs = $data->result->data->tab;
        $mainPages = array();

        foreach($menu as $item) {
            if ($item["has_sub"] === 1) {
                if($item["sub_type"] == 0) {
                    $subItems = $this->getSubItemsForPage($tabs, $item["sub_menu"]);
                    $mainPages[] = array(
                        "id" => $item["link"],
                        "name" => $item["name"],
                        "sub_items" => $subItems
                    );
                } else{
                    foreach($item["sub_menu"] as $sub_item){
                        if($sub_item["has_sub"] == 1){
                            if($sub_item["sub_type"] == 0) {
                                $subSubItems = $this->getSubItemsForPage($tabs, $sub_item["sub_sub_menu"]);
                                $mainPages[] = array(
                                    "id" => $sub_item["link"],
                                    "name" => $sub_item["name"],
                                    "sub_items" => $subSubItems
                                );
                            }
                        }
                    }
                }
            }
        }

        return $mainPages;
    }

    public function getSubItemsForPage($tabs, $items){
        $subItems= array();
        foreach($items as $item){
            foreach($tabs as $tab){
                if($tab->id == $item["link"]){
                    $subItems[] = array(
                        "id" => $tab->id,
                        "name" => $tab->web_name,
                        "content" => $this->getContentForSubItems($tab)
                    );
                }
            }
        }
        return $subItems;
    }

    public function getContentForSubItems($tab){
        $content = array();
        if($tab->type->name == "content"){
            foreach($tab->unified_content->item as $item){
                switch($item->type->name){
                    case "image":
                        $img = array();
                        foreach($item->image as $image){
                            $img[] = array(
                                "src" => $image->src,
                                "alt" => $image->title
                            );
                        }
                        $content[] = array(
                            "type" => $item->type->name,
                            "img" => $img
                        );
                        break;
                    case "html":
                        $content[] = array(
                            "type" => $item->type->name,
                            "text" => $item->text
                        );
                        break;
                    case "text":
                        $content[] = array(
                            "type" => $item->type->name,
                            "text" => $item->text
                        );
                        break;
                }
            }
        }
        return $content;
    }
    
        public function getAboutUsSection($data){
        $section = $this->getTabByClass($data->result->data->tab, "about");

        $sectionArray = array();

        foreach($section->menu->item as $item){
            $tab = $this->findTabByID($data->result->data->tab, $item->tab);

            $content = array();
            foreach($tab->unified_content->item as $page){
                switch($page->type->name){
                    case "header":
                        $content["title"] = $page->text;
                        break;
                    case "image":
                        $content["img"] = $page->image[0]->src;
                        break;
                    case "text":
                        $content["text"] =  $page->text;
                        break;
                    case "html":
                        $content["html"] =  $page->text;
                        break;
                }
            }
            $sectionArray[] = $content;

        }
        
        return $sectionArray;
    }

    public function getAboutUsSectionTitle($data){
        $section = $this->getTabByClass($data->result->data->tab, "about");
        if($section)
            return $section->web_name;
        else
            return null;
    }
}