<?php

namespace Model;

use Nette\Utils\Json;

class AdamModel {
    
    public function getJSON($app_id) {
	$header = get_headers('http://my.adamapp.com/export/web/'.$app_id.'.json');
	if(!$header || $header[0] == 'HTTP/1.1 404 Not Found') {
	    return false;
	} else {
	    $json = file_get_contents('http://my.adamapp.com/export/web/'.$app_id.'.json');
	    $data = Json::decode($json);
	    return $data;
	}
    }
    
    
    public function getAppID($data) {
	foreach($data->result->data->language as $language) {
            return $language->app_id;
        }  
    }
    
    
    public function getAppName($app_id) {
	$data = $this->getJSON($app_id);
	return $data->result->data->app_name;
    }
    
   
    public function getImages($array) {
	$img = [];
	foreach($array as $image) {
	    $img[] = '<img src="http://my.adamapp.com/'.$image->src.'">';
	}
	return $img;
    }
    
    
    public function getPlaceholder($data,$tab) {
	$images = $this->getArrayImages($tab);
	if($images) {
	    $image = reset($images);
	    $url = parse_url($image->src);
	    if(isset($url['scheme']) && $url['scheme'] == 'http') {
		return $image->src;
	    } elseif(isset($url['scheme']) && $url['scheme'] == 'https') {
		return $image->src;
	    } else {
		return 'http://my.adamapp.com/'.$image->src;
	    }   
	} else {
	    if(isset($data->result->data->config->placeholder->src)) {
		return 'http://my.adamapp.com/'.$data->result->data->config->placeholder->src;
	    } else {
		return null;
	    }
	}
    }
    
    
    public function getArrayImages($tab) {
	if(isset($tab->unified_content->item)) {
	    foreach ($tab->unified_content->item as $item) {
		if($item->type->name == 'image') {
		    return $item->image;
		}
	    }
	}
    }
    
    
    public function getCategory($data,$id) {
	$category = $this->findCategory($data);
	foreach($category as $cat) {
	    if($cat['id'] == $id) {
		return $cat['name'];
	    } elseif($cat['has_sub'] == 1) {
		foreach($cat['sub'] as $sub) {
		    if($sub['id'] == $id) {
			return $cat['name'];
		    } elseif($sub['has_sub'] == 1) {
			foreach($sub['subsub'] as $subsub){
			    if($subsub['id'] == $id) {
				return $sub['name'];
			    }
			}
		    }
		}
	    } 
	}
    }

    
    public function findCategory($data) {
	$main_cat = $data->result->data->main_tab;
	$tabs = $data->result->data->tab;
	$category = [];
	foreach ($tabs as $tab) {
	    if($tab->id == $main_cat) {
		foreach($tab->menu->item as $item) {
		    
		    $sub = $this->findSubCategory($tabs, $item->tab);
		    
		    if(empty($sub)) {
			$has_sub = 0;
		    } else {
			$has_sub = 1;
		    }
		    
		    $category[] = [
			'id' => $item->tab,
			'name' => $item->name,
			'has_sub' => $has_sub,
			'sub' => $sub
		    ];
		}
	    }
	}
	return $category;
    }
    
    
    public function findSubCategory($tabs,$parentID) {
	$sub_cat = [];
	foreach ($tabs as $tab) {
	    if(isset($tab->menu->item)) {
		foreach($tab->menu->item as $item) {
		    if($parentID == $tab->id) {
			$subsub = $this->findSubSubCategory($tabs, $item->tab);
			
			if(empty($subsub)){
			    $has_sub = 0;
			} else {
			    $has_sub = 1;
			}
		    
			$sub_cat[] = [
			    'id' => $item->tab,
			    'name' => $item->name,
			    'has_sub' => $has_sub,
			    'subsub' => $subsub 
			];
		    }
		}
	    }
	}
	return $sub_cat;
    }
    
    
    public function findSubSubCategory($tabs,$parentSubID) {
	$subsub = [];
	foreach($tabs as $tab) {
	    if($parentSubID == $tab->id && $tab->type->name == 'menu') {
		foreach($tab->menu->item as $item) {
		    $subsub[] = [
			'id' => $item->tab,
		    ];
		}
	    }
	}
	return $subsub;
    }
    
    
    public function getTime($tab) {
	if(isset($tab->published_date)) {
	    return $tab->published_date;
	} else {
	    return date('Y-m-d');
	}
    }
    
    
    public function getContent($data) {
	foreach ($data->result->data->tab as $tab) {
	    $itemdata = [];
	    if($tab->type->name == 'content' && $tab->web_class != 'claim' && !empty($tab->unified_content->item) && $tab->web_class != 'gallery' && $tab->web_class != 'splitter') {
		
		// Titulek
		$itemdata['title'] = $tab->web_name;

		// Kategorie
		$itemdata['category'] = $this->getCategory($data,$tab->id);

		// Úvodní obrázek
		$itemdata['placeholder'] = $this->getPlaceholder($data,$tab);
		
		// ID příspěvku
		$itemdata['id'] = $tab->id;
		
		// Čas vytvoření
		$itemdata['pubDate'] = $this->getTime($tab);

		// Popis
		// Obrázky
		// HTML	
		// Button - call, mail, url, facebook		    
		foreach($tab->unified_content->item as $item) {
		    switch ($item->type->name){
			case 'header':
			    $itemdata[] = ['description' => $item->text];
			    break;   
			case 'text':
			    $itemdata[] = ['description' => $item->text];
			    break;  
			case 'html':
			    $itemdata[] = ['description' => $item->text];
			    break; 
			case 'image':
			    $itemdata['image'] = $this->getImages($item->image);
			    break; 
			case 'button':
			    switch($item->action->type->name) {
				case 'call':
				    $itemdata[] = ['description' => '<a href="tel:'.$item->action->link.'">'.$item->action->link.'</a>'];
				    break;
				case 'mail':
				    $itemdata[] = ['description' => '<a href="mailto:'.$item->action->link.'">'.$item->action->link.'</a>'];
				    break;
				case 'url':
				    $itemdata[] = ['description' => '<a href="http://'.$item->action->link.'">'.$item->action->link.'</a>'];
				    break;
				case 'facebook':
				    $itemdata[] = ['description' => '<a href="'.$item->action->link.'">Facebook</a>'];
				    break;
			    }
			    break;
		    }
		}
		$output[] = $itemdata;
		
	    } elseif($tab->type->name == 'link') {
		
		// Titulek
		$itemdata['title'] = $tab->web_name;
		
		// Kategorie
		$itemdata['category'] = $this->getCategory($data,$tab->id);
		
		// Úvodní obrázek
		$itemdata['placeholder'] = '<img src="'.$this->getPlaceholder($data,$tab).'">';
		
		// ID příspěvku
		$itemdata['id'] = $tab->id;
		
		// Čas vytvoření
		$itemdata['pubDate'] = $tab->published_date;
		
		// Link
		$itemdata['link'] = '<a href="'.$tab->link.'">'.$tab->link.'</a>';
		
		$output[] = $itemdata;
	    }
	}
	return array_filter($output);
    }
        
}

