<?php

namespace Model;

use Nette\Utils\Json;

class AdamWordpress {   

    public function getData($appid) {  
	$header = get_headers('http://my.adamapp.com/export/web/'.$appid.'.json');
	if(!$header || $header[0] == 'HTTP/1.1 404 Not Found') {
	    return false;
	} else {
	    $json = file_get_contents('http://my.adamapp.com/export/web/'.$appid.'.json');
	    $data = Json::decode($json, true);
	    return $data;
	}
    }
    
    
    public function getAppId($data) {
        foreach($data['result']['data']['language'] as $language) {
            return $language['app_id'];
        }      
    }
    
    
    public function getAppName($appid) {
	$data = $this->getData($appid);
	return $data['result']['data']['app_name'];
    }
    
    
    public function getPlaceholder($appid) {
	$data = $this->getData($appid);
	if(isset($data['result']['data']['config']['placeholder']['src'])) {	    
	    $placeholder = 'http://my.adamapp.com/'.$data['result']['data']['config']['placeholder']['src'];
	    return $placeholder;
	}
    }


    public function getImages($array) {
	$images = [];
        foreach ($array as $image) {
            $images[] = [
                'image' => '<img src="http://my.adamapp.com/'. $image['src'] . '">'
            ];
        }
        return $images;
    }
    
    
    public function getFirstImage($images) {
	$image = reset($images);
	return 'http://my.adamapp.com/'.$image['src'];
    }
    

    public function getCategory($data) {
	$tabs = $data['result']['data']['tab'];
	$main_tab = $data['result']['data']['main_tab'];
	$cat = [];
	foreach($data['result']['data']['tab'] as $tab) {
	    if($tab['id'] == $main_tab) {
		foreach($tab['menu']['item'] as $item) {
		    $sub = $this->getSubCategory($tabs, $item['tab']);
		    $cat[] = [
			'id' => $item['tab'],
			'name' => $item['name'],
			'sub_cat' => $sub
		    ];
		}
	    } 
	}
	return $cat;
    }
    
    public function getSubCategory($tabs,$parentID) {
	$sub = [];
	foreach($tabs as $tab) {
	    $type = "";
            if(isset($tab['type'])){
                $type = $tab['type']['name'];
            }
	    if($tab['id'] == $parentID && ($type === "menu")){
		foreach($tab['menu']['item'] as $item) {
		    $sub[] = ['id' => $item['tab']];
		}
	    }
	}	    
	return $sub;
    }
    
    
    public function findCategoryByID($category, $id) {
	foreach($category as $cat) {
            if ($cat['id'] == $id) {
                return ['category' => $cat['name']]; 
            } else {
                foreach($cat['sub_cat'] as $sub) {
                    if ($sub['id'] == $id) {
                        return ['category' => $cat['name']];
                    }
                }
            }		
	}
    }
    
    public function getTypeHeader($tab) {
        if (isset($tab['unified_content'])) {
            foreach($tab['unified_content']['item'] as $item) {
                if($item['type']['name'] == "header") {
                    return $item;
                }
            }
        }
    }
    
    public function getTypeImages($tab) {
        if (isset($tab['unified_content'])) {
            foreach($tab['unified_content']['item'] as $item) {
                if($item['type']['name'] == "image") {
                    return $item;
                }
            }
        }
    }
   

    public function findLink($tabs) {
	if(isset($tabs['link'])) {
	    return $tabs['link'];
	}
    }
    
    
    public function getContent($data) {
	$category = $this->getCategory($data);
        foreach ($data['result']['data']['tab'] as $tab) {
	    $itemdata = [];
	    if ($tab['web_class'] != "gallery" && $tab['web_class'] != "claim" && $tab['web_class'] != "splitter" && $tab['type']['name'] != 'link') {
		   
		    $cat = $this->findCategoryByID($category,$tab['id']);
		    if(isset($cat)) {
			$itemdata[] = $cat;
		    }

		    $item = $this->getTypeHeader($tab);
		    if(isset($item)) {
			$itemdata[] = ['title' => $item['text']];
		    } else {
			$itemdata[] = ['title' => $tab["web_name"]];
		    }

		    //$itemdata[] = ['link' => $this->findLink($tab)];
		    
		    $placeholder = $this->getTypeImages($tab);
		    if($placeholder) {
			$itemdata[] = ['placeholder' => $this->getFirstImage($placeholder['image'])];
		    } else {
			$itemdata[] = ['placeholder' => 'http://my.adamapp.com/'.$data['result']['data']['config']['placeholder']['src']];
		    }

		    if (isset($tab['unified_content']['item']) && $tab['unified_content']['item'] != []) {
			foreach ($tab['unified_content']['item'] as $item) {
			    $type = $item['type']['name'];			    
			    switch ($type) {
				case "header":
				    $itemdata[] = ['header' => $item['text']];
				    break;
				case "text":
				    $itemdata[] = ['description' => $item['text']];
				    break;
				case "image":
				    $itemdata[] = $this->getImages($item['image']);			
				    break;
				case "html":
				    $itemdata[] = ['description' => '<div>' . $item['text'] . '</div>'];
				    break;
//				case "form":
//				    $itemdata[] = ['form' => $this->parseFormItem($item)];
//				     break;
				case "button":
				    if(isset($item['action']['type']['name'])) {
					if ($item['action']['type']['name'] == "call") {
					    $itemdata[] = ['description' => '<a href="tel:' . $item['action']['link'] . '">' . $item['action']['link'] . '</a>'];
					} elseif ($item['action']['type']['name'] == "mail") {
					    $itemdata[] = ['description' => '<a href="mailto:' . $item['action']['link'] . '">' . $item['action']['link'] . '</a>'];
					} elseif ($item['action']['type']['name'] == "url") {
					    $itemdata[] = ['description' => '<a href="http://' . $item['action']['link'] . '">' . $item['action']['link'] . '</a>'];
					} elseif ($item['action']['type']['name'] == "facebook") {
					    $itemdata[] = ['description' => '<a href="' . $item['action']['link'] . '">Facebook</a>'];
					}
				    }
				    break;
			    }
			}

			if(isset($tab['published_date'])) {
			    $itemdata[] = ['pubDate' => $tab['published_date']];
			}

			$itemdata[] = ['id' => $tab['id']];
			$array[] = $itemdata;
		    }
            } elseif ($tab['type']['name'] == 'link' && $tab['type']['link_type'] != 'external') {
		$itemdata[] = ['title' => $tab['web_name']];
		$cat = $this->findCategoryByID($category,$tab['id']);
		if(isset($cat)) {
		    $itemdata[] = $cat;
		}
		$itemdata[] = ['placeholder' => 'http://my.adamapp.com/'.$data['result']['data']['config']['placeholder']['src']];
		$itemdata[] = ['description' => '<iframe src='.$tab['link'].' width="100%"></iframe>'];
		$array[] = $itemdata;
	    }
        }
        return array_filter($array);
    }
    
    // Testování Filtru
//    public function testing() {
//	$data = $this->getData();
//	$tabs = $data['result']['data']['tab'];
//	$category = $this->getCategory($data);
//	foreach($data['result']['data']['tab'] as $tab) {
//            dump($category);
//	    dump($this->findCategoryByID($category, $tab["id"]));
//	}
//    }

}