<?php

namespace App\Presenters;

use Nette\Utils\Finder;

class AutoloadPresenter extends BasePresenter {
    
     /** @var \Model\AdamModel @inject */
    public $adamModel;
    
    /** Auto cron function - load all exist XML and update them */
    public function renderLoad() {
	$appid_array = [];
	foreach(Finder::findFiles('*.txt')->in(__DIR__.'/../../www/xml/') as $file) {
	    $appid_array = [file_get_contents($file)];
	    foreach ($appid_array as $appid) {
		$data = $this->adamModel->getJSON($appid);
		if($data) {
		    $this->template->content = $this->adamModel->getContent($data);
		    $app_id = $this->adamModel->getAppId($data);
		    $this->template->app_name = $this->adamModel->getAppName($app_id);
		    if($app_id == $appid) {
			$template = $this->getTemplate();
			$filename = __DIR__.'/../../www/xml/'.$app_id;    
			    if(file_exists(__DIR__.'/../../www/xml/'.$app_id.'/'.$app_id.'.xml')) {
				file_put_contents(__DIR__.'/../../www/xml/'.$app_id.'/'.$app_id.'.xml', $template->setFile(__DIR__.'/templates/Homepage/rss.latte'));
			    }     
		    }
		}
	    }
	}
    }
}

