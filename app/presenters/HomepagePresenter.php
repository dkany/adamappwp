<?php

namespace App\Presenters;

class HomepagePresenter extends BasePresenter {

    /** @var \Forms\FormFactory @inject */
    public $formFactory;
    
    /** @var \Model\AdamModel @inject */
    public $adamModel;
    
    public function createComponentAppForm() {
	$form = $this->formFactory->create();
	$form->addText('app_id',null)
	     ->setHtmlAttribute('class', 'appid_search')
	     ->setHtmlAttribute('placeholder', 'Zadejte ID aplikace')
	     ->setRequired('Vyplňte ID aplikace.');
	$form->addSubmit('send','Odeslat')
	     ->setHtmlAttribute('class', 'submit_button');
	$form->onSuccess[] = [$this,'formSuccess'];
	return $form;
    }
    
    public function formSuccess($form, $values) {	
	$data = $this->adamModel->getJSON($values->app_id);
	if($data) {
	    $this->template->content = $this->adamModel->getContent($data);
	    $app_id = $this->adamModel->getAppId($data);
	    $this->template->app_name = $this->adamModel->getAppName($app_id);
	    if($app_id == $values->app_id) {
		$template = $this->getTemplate();
		$filename = __DIR__.'/../../www/xml/'.$app_id;    
		if($app_id) {
		    if(file_exists(__DIR__.'/../../www/xml/'.$app_id.'/'.$app_id.'.xml')) {
			file_put_contents(__DIR__.'/../../www/xml/'.$app_id.'/'.$app_id.'.xml', $template->setFile(__DIR__.'/templates/Homepage/rss.latte'));
		    } else {
			mkdir(__DIR__.'/../../www/xml/'.$app_id);
			file_put_contents(__DIR__.'/../../www/xml/'.$app_id.'/'.$app_id.'.xml', $template->setFile(__DIR__.'/templates/Homepage/rss.latte'));
			file_put_contents(__DIR__.'/../../www/xml/'.$app_id.'.txt', $app_id);
		    }         
		}
		$this->redirect('Link:default',$app_id);
		$this->redirect('this');
	    } 
	} else {
	    $this->flashMessage('ID aplikace neexistuje, zkontrolujte v my.adamapp.com, zda v přehledu ID existuje.','error');
	}	
    }
}

