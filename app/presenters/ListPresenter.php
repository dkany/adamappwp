<?php

namespace App\Presenters;

use Nette\Utils\Finder;

class ListPresenter extends BasePresenter {
    
    /** @var \Model\AdamModel @inject */
    public $adamModel;
    
    public function renderShow() {
	$appid_array = [];
	$items = [];
	foreach(Finder::findFiles('*.txt')->in(__DIR__.'/../../www/xml/') as $file) {
	    $appid_array = [file_get_contents($file)];
	    foreach ($appid_array as $appid) {
		$items['appname'] = $this->adamModel->getAppName($appid);
		$items['path'] = '/xml/'.$appid.'/'.$appid.'.xml';
		$array[] = $items;
		$this->template->items = $array;
	    } 
	}
    }
}

