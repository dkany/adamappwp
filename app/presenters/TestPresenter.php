<?php

namespace App\Presenters;

class TestPresenter extends BasePresenter {

    /** @var \Forms\FormFactory @inject */
    public $formFactory;
    
    /** @var \Model\AdamModel @inject */
    public $adamModel;
    
    public function createComponentAppForm() {
	$form = $this->formFactory->create();
	$form->addText('app_id',null)
	     ->setHtmlAttribute('class', 'appid_search')
	     ->setHtmlAttribute('placeholder', 'Zadejte ID aplikace')
	     ->setRequired('Vyplňte ID aplikace.');
	$form->addSubmit('send','Odeslat')
	     ->setHtmlAttribute('class', 'submit_button');
	$form->onSuccess[] = [$this,'formSuccess'];
	return $form;
    }
    
    public function formSuccess($form, $values) {
	$json = $this->adamModel->getJSON($values->app_id);
	$content = $this->adamModel->getContent($json);
	dump($content);
    }
 
}

