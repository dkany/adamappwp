<?php
// source: C:\xampp\htdocs\adamappwp\app\presenters/templates/Homepage/rss.latte

use Latte\Runtime as LR;

class Template067e43afb9 extends Latte\Runtime\Template
{
	public $contentType = 'xml';


	function main()
	{
		extract($this->params);
		if (empty($this->global->coreCaptured) && in_array($this->getReferenceType(), ["extends", NULL], TRUE)) header('Content-Type: application/xml');
		?><<?php ?>?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0">
	<channel>
	    <title><?php echo LR\Filters::escapeXml($app_name) /* line 36 */ ?></title>
<?php
		$iterations = 0;
		foreach ($content as $item) {
?>
		<item>
		    <title><?php
			if (isset($item['title'])) {
				echo LR\Filters::escapeXml($item['title']) /* line 39 */;
			}
?></title>
		    <category><?php
			if (isset($item['category'])) {
				echo LR\Filters::escapeXml($item['category']) /* line 40 */;
			}
?></category>
		    <image><?php
			if (isset($item['placeholder'])) {
				?> <?php
				echo LR\Filters::escapeXml($item['placeholder']) /* line 41 */;
			}
?></image>
		    <description>
				
<?php
			$iterations = 0;
			foreach ($item as $data) {
				?>			    <?php
				if (isset($data['description'])) {
					echo LR\Filters::escapeXml($data['description']) /* line 49 */;
				}
?>

<?php
				$iterations++;
			}
			?>			<?php
			if (isset($item['link'])) {
				?> <?php
				echo LR\Filters::escapeXml($item['link']) /* line 51 */;
			}
?>

		    </description>
		    <guid><?php
			if (isset($item['id'])) {
				echo LR\Filters::escapeXml($item['id']) /* line 53 */;
			}
?></guid>
		    <pubDate><?php
			if (isset($item['pubDate'])) {
				echo LR\Filters::escapeXml($item['pubDate']) /* line 54 */;
			}
?></pubDate>
		</item>
<?php
			$iterations++;
		}
?>
	</channel>
</rss><?php
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['data'])) trigger_error('Variable $data overwritten in foreach on line 48');
		if (isset($this->params['item'])) trigger_error('Variable $item overwritten in foreach on line 37');
		Nette\Bridges\ApplicationLatte\UIRuntime::initialize($this, $this->parentName, $this->blocks);
		
	}

}
