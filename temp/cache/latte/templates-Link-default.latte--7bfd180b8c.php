<?php
// source: C:\xampp\htdocs\adamappwp\app\presenters/templates/Link/default.latte

use Latte\Runtime as LR;

class Template7bfd180b8c extends Latte\Runtime\Template
{
	public $blocks = [
		'content' => 'blockContent',
	];

	public $blockTypes = [
		'content' => 'html',
	];


	function main()
	{
		extract($this->params);
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('content', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		Nette\Bridges\ApplicationLatte\UIRuntime::initialize($this, $this->parentName, $this->blocks);
		
	}


	function blockContent($_args)
	{
		extract($_args);
?>
<div class="content" id="main">
    <center>
	<div class="center_wrapp"> 
	    <h1>Propisování proběhlo úspěšně.</h1>
	    <div>
		Tento odkaz <div class="src_link"><a href="<?php
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($baseUri)) /* line 7 */;
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($path)) /* line 7 */ ?>"><?php
		echo LR\Filters::escapeHtmlText($baseUri) /* line 7 */;
		echo LR\Filters::escapeHtmlText($path) /* line 7 */ ?></a></div> zkopírujte do Wordpressu.
	    </div>
	</div>
	    <br>
	    <a href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("Homepage:default")) ?>">Vrátit se na hlavní stránku</a>
    </center>
</div><?php
	}

}
