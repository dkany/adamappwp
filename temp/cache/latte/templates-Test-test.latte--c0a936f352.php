<?php
// source: C:\xampp\htdocs\adamappwp\app\presenters/templates/Test/test.latte

use Latte\Runtime as LR;

class Templatec0a936f352 extends Latte\Runtime\Template
{
	public $blocks = [
		'content' => 'blockContent',
	];

	public $blockTypes = [
		'content' => 'html',
	];


	function main()
	{
		extract($this->params);
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('content', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		Nette\Bridges\ApplicationLatte\UIRuntime::initialize($this, $this->parentName, $this->blocks);
		
	}


	function blockContent($_args)
	{
		extract($_args);
?>
<div class="content" id="main">
    <center>
	<div class="center_wrapp">    
	    <div class="adam_logo"><h2>Test output</h2></div>
		<!-- <h1>Zadejte ID mobilní aplikace</h1> -->
		<div class="adam_home">
<?php
		/* line 8 */ $_tmp = $this->global->uiControl->getComponent("appForm");
		if ($_tmp instanceof Nette\Application\UI\IRenderable) $_tmp->redrawControl(null, false);
		$_tmp->render();
?>
		</div>
	</div>
    </center>
</div>
<?php
	}

}
